SELECT t.country, t.total_cases, a.txt, b.txt1, c.txt2, d.txt3, e.txt4, f.txt5, g.txt6, h.txt7, b.country, b.total_cases
from top_20 t inner join bottom_20 b
on t.ids = b.ids
 

left join (
select 2 sn, 'Total Case:' txt from dual
union
select 3, ''|| sum(total_cases) from covid
union
select 9, 'Active' from dual
union
select 10, 'Cases' from dual
) a
on t.ids = a.sn
 

left join (
select 14 sn, details.total_mild_cases || '(' || details.mild_percentage || '%)' txt1 from details
union
select 15, 'in Mild Condition' from dual
) b
on t.ids = b.sn
 

left join (
select 10 sn, ''|| total_active_cases txt2 from details
union
select 11, 'Currently Infected Patients' from dual
) c
on t.ids = c.sn
 

left join (
select 14 sn, ''|| total_serious_cases || '(' || serious_percentage || '%)' txt3 from details
union
select 15, 'Serious or Critical' from dual
) d
on t.ids = d.sn
 

left join (
select 2 sn, 'Deaths :' txt4 from dual
union
select 3, ''|| total_deaths from details
) e
on t.ids = e.sn
 

left join (
select 14 sn, ''|| total_recovered || '(' || recovered_percentage || '%)' txt5 from details
union
select 15, 'Recovered/Discharged' from dual
) f
on t.ids = f.sn
 

left join (
select 2 sn, 'Recovered:' txt6 from dual
union
select 3, ''|| total_recovered from details
union
select 8, 'Closed Cases:' from dual
union
select 10, ''|| total_closed_cases from details
union
select 11, 'Cases which had an outcomes' from dual
) g
on t.ids = g.sn
 

left join (
select 14 sn, '' || total_deaths || '('|| death_percentage ||'%)' txt7 from details
union
select 15, 'Deaths' from dual
) h
on t.ids = h.sn

order by t.ids;
