CREATE OR REPLACE PROCEDURE totalSum IS

    v_cases NUMBER(20);
    v_deaths NUMBER(20);
    v_recovered NUMBER(20);
    v_serious NUMBER(20);
    v_active NUMBER(20);
    v_closed NUMBER(20);
    v_mild NUMBER(20);

    v_mild_per NUMBER(10,2);
    v_serious_per NUMBER(10,2);
    v_recovered_per NUMBER(10,2);
    v_death_per NUMBER(10,2);

BEGIN   
 

    EXECUTE IMMEDIATE 'SELECT SUM(total_cases) FROM covid' INTO v_cases;
    EXECUTE IMMEDIATE 'SELECT SUM(deaths) FROM covid' INTO v_deaths;
    EXECUTE IMMEDIATE 'SELECT SUM(recovered) FROM covid' INTO v_recovered; 
    EXECUTE IMMEDIATE 'SELECT SUM(serious) FROM covid' INTO v_serious;
    EXECUTE IMMEDIATE 'SELECT SUM(total_cases) - SUM(deaths) - SUM(recovered) FROM covid' INTO v_active;
    EXECUTE IMMEDIATE 'SELECT SUM(deaths) + SUM(recovered) FROM covid' INTO v_closed;
    EXECUTE IMMEDIATE 'SELECT SUM(total_cases) - SUM(deaths) - SUM(recovered) - SUM(serious) FROM covid' INTO v_mild;
    EXECUTE IMMEDIATE 'SELECT ROUND((SUM(total_cases) - SUM(deaths) - SUM(recovered) - SUM(serious))/(SUM(total_cases) - SUM(deaths) - SUM(recovered))*100,1) FROM covid' INTO v_mild_per;
    EXECUTE IMMEDIATE 'SELECT 100 - (ROUND((SUM(total_cases) - SUM(deaths) - SUM(recovered) - SUM(serious))/(SUM(total_cases) - SUM(deaths) - SUM(recovered))*100,1)) FROM covid' INTO v_serious_per;
    EXECUTE IMMEDIATE 'SELECT ROUND((SUM(recovered)) / (SUM(deaths) + SUM(recovered)) * 100) FROM covid' INTO v_recovered_per;   
    EXECUTE IMMEDIATE 'SELECT 100 - (ROUND((SUM(recovered)) / (SUM(deaths) + SUM(recovered)) * 100)) FROM covid' INTO v_death_per;

   

    INSERT INTO details (total_cases, total_deaths, total_recovered, total_active_cases, total_closed_cases, total_mild_cases,
                         total_serious_cases, serious_percentage, mild_percentage, death_percentage, recovered_percentage)
    VALUES              (v_cases, v_deaths, v_recovered, v_active, v_closed, v_mild,
                         v_serious, v_serious_per, v_mild_per, v_death_per, v_recovered_per);  

END;
/

 

EXEC totalSum;
