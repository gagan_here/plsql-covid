CREATE OR REPLACE VIEW top_20 AS
SELECT row_number() over (order by total_cases desc) ids, c.country, c.total_cases
FROM covid c
FETCH FIRST 20 ROWS ONLY;
 

CREATE OR REPLACE VIEW bottom_20 AS
SELECT row_number() over (order by total_cases ASC) ids, c.country, c.total_cases
FROM covid c
FETCH FIRST 20 ROWS ONLY;


CREATE OR REPLACE PROCEDURE updateInfo(nCases NUMBER,dCases NUMBER,rCases NUMBER ,sCases NUMBER, country VARCHAR)
IS

    BEGIN
    
    UPDATE covid
    SET total_cases= total_cases+ nCases , deaths= deaths+dCases, recovered= recovered+rCases, serious= serious + sCases
    WHERE country_name = country;
    
END;
/
